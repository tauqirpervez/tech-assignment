﻿using NUnit.Framework;
using PhotoProducts.Api.Controllers;
using Moq;
using Microsoft.AspNetCore.Routing;
using PhotoProducts.Api.Data.Repository;
using System.Collections.Generic;
using PhotoProducts.Api.Model;
using Microsoft.AspNetCore.Mvc;
using PhotoProducts.Api.Test.Utilities;

namespace PhotoProducts.Api.Test.Controllers
{
    [TestFixture]
    internal class OrderControllerTest
    {
        private Mock<LinkGenerator> linkGeneratorStub;
        private Mock<IOrderRepository> orderRepositoryStub;

        [SetUp]
        public void OnSetUp()
        {
            linkGeneratorStub = new Mock<LinkGenerator>();
            orderRepositoryStub = new Mock<IOrderRepository>();
        }

        [Test]
        public void Get_ReturnsOkResultWithAllOrders()
        {
            //Arrange
            var ordersStub = new Mock<IList<Order>>();
            orderRepositoryStub.Setup(o => o.GetAll()).Returns(ordersStub.Object);

            //Act
            var orderController = new OrderController(orderRepositoryStub.Object, linkGeneratorStub.Object);
            var actionResult = orderController.Get();

            //Assert
            var okResult = actionResult.Result as OkObjectResult;
            Assert.That(okResult, Is.Not.Null);
            Assert.That(okResult.Value, Is.EqualTo(ordersStub.Object));
            orderRepositoryStub.VerifyAll();
        }

        [Test]
        public void GetById_IdExists_ReturnsOkResultWithOrder()
        {
            //Arrange
            var testId = 5;
            var orderStub = new Mock<Order>();
            orderRepositoryStub.Setup(o => o.GetById(testId)).Returns(orderStub.Object);

            //Act
            var orderController = new OrderController(orderRepositoryStub.Object, linkGeneratorStub.Object);
            var actionResult = orderController.Get(testId);

            //Assert
            var okResult = actionResult.Result as OkObjectResult;
            Assert.That(okResult, Is.Not.Null);
            Assert.That(okResult.Value, Is.EqualTo(orderStub.Object));
            orderRepositoryStub.VerifyAll();
        }

        [Test]
        public void GetById_IdDoesNotExist_ReturnsNotFoundResult()
        {
            //Arrange
            var testId = 5;
            var orderStub = new Mock<Order>();
            orderRepositoryStub.Setup(o => o.GetById(testId)).Returns((Order)null);

            //Act
            var orderController = new OrderController(orderRepositoryStub.Object, linkGeneratorStub.Object);
            var actionResult = orderController.Get(testId);

            //Assert
            var notFoundResult = actionResult.Result as NotFoundObjectResult;
            Assert.That(notFoundResult, Is.Not.Null);
            Assert.That(notFoundResult.Value, Is.EqualTo(testId));
            orderRepositoryStub.VerifyAll();
        }

        [Test]
        public void Create_NewOrderid_OrderCreated()
        {
            //Arrange
            var testId = 5;
            var testBinWidth = 19;
            var testUri = "test";
            var order = new Order { OrderId = testId, Items = new List<OrderItem> { new OrderItem { ProductType = "photoBook", Quantity = 1 } } };
            orderRepositoryStub.Setup(o => o.GetById(testId)).Returns((Order)null);
            orderRepositoryStub.Setup(o => o.Create(order));
            var linkGenerator = new TestLinkGenerator(testUri);

            //Act
            var orderController = new OrderController(orderRepositoryStub.Object, linkGenerator);
            var actionResult = orderController.Post(order);

            //Assert
            var createdResult = actionResult.Result as CreatedResult;
            Assert.That(createdResult, Is.Not.Null);
            Assert.That(createdResult.Value, Is.EqualTo(testBinWidth));
            orderRepositoryStub.VerifyAll();
        }

        [Test]
        public void Create_ExistingOrderid_ReturnsBadRequest()
        {
            //Arrange
            var testId = 5;
            var order = new Order { OrderId = testId, Items = new List<OrderItem> { new OrderItem { ProductType = "photoBook", Quantity = 1 } } };
            orderRepositoryStub.Setup(o => o.GetById(testId)).Returns(order);

            //Act
            var orderController = new OrderController(orderRepositoryStub.Object, linkGeneratorStub.Object);
            var actionResult = orderController.Post(order);

            //Assert
            var badRequest = actionResult.Result as BadRequestObjectResult;
            Assert.That(badRequest, Is.Not.Null);
            orderRepositoryStub.VerifyAll();
        }
    }
}
