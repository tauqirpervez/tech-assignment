﻿using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Http;

namespace PhotoProducts.Api.Test.Utilities
{
    internal class TestLinkGenerator : LinkGenerator
    {
        private readonly string _testUri;

        public TestLinkGenerator(string testUri)
        {
            this._testUri = testUri;
        }

        public override string GetPathByAddress<TAddress>(HttpContext httpContext, TAddress address, RouteValueDictionary values, RouteValueDictionary ambientValues = null, PathString? pathBase = null, FragmentString fragment = default, LinkOptions options = null)
        {
            throw new System.NotImplementedException();
        }

        public override string GetPathByAddress<TAddress>(TAddress address, RouteValueDictionary values, PathString pathBase = default, FragmentString fragment = default, LinkOptions options = null)
        {
            return _testUri;
        }

        public override string GetUriByAddress<TAddress>(HttpContext httpContext, TAddress address, RouteValueDictionary values, RouteValueDictionary ambientValues = null, string scheme = null, HostString? host = null, PathString? pathBase = null, FragmentString fragment = default, LinkOptions options = null)
        {
            throw new System.NotImplementedException();
        }

        public override string GetUriByAddress<TAddress>(TAddress address, RouteValueDictionary values, string scheme, HostString host, PathString pathBase = default, FragmentString fragment = default, LinkOptions options = null)
        {
            throw new System.NotImplementedException();
        }
    }
}
