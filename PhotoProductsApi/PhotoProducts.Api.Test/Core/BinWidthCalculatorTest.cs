﻿using NUnit.Framework;
using PhotoProducts.Api.Core;
using PhotoProducts.Api.Model;
using System.Collections.Generic;

namespace PhotoProducts.Api.Test.Core
{
    [TestFixture]
    internal class BinWidthCalculatorTest
    {
        private const string s_PhotoBookProductType = "photoBook";
        private const string s_CalendarProductType = "calendar";
        private const string s_CanvasProductType = "canvas";
        private const string s_CardsProductType = "cards";
        private const string s_MugProductType = "mug";

        [TestCase(s_PhotoBookProductType, 19)]
        [TestCase(s_CalendarProductType, 10)]
        [TestCase(s_CanvasProductType, 16)]
        [TestCase(s_CardsProductType, 4.7)]
        [TestCase(s_MugProductType, 94)]
        public void CalculateBinWidth_OfEachProduct_ReturnsExpectedBinWidth(string productType, double expectedBinWidth)
        {
            //Arrange
            var orderItems = new List<OrderItem>
            {
                    CreateOrderItem(productType, 1)
            };

            //Act
            var binWidth = BinWidthCalculator.Calculate(orderItems);

            //Assert
            Assert.That(binWidth, Is.EqualTo(expectedBinWidth));

        }

        [TestCase(s_MugProductType, 2, 94)]
        [TestCase(s_MugProductType, 4, 94)]
        [TestCase(s_MugProductType, 5, 188)]
        [TestCase(s_MugProductType, 8, 188)]
        [TestCase(s_MugProductType, 9, 282)]
        public void CalculateBinWidth_OfStackableProducts_ReturnsExpectedBinWidth(string productType, int quantity, double expectedBinWidth)
        {
            //Arrange
            var orderItems = new List<OrderItem>
            {
                    CreateOrderItem(productType, quantity)
            };

            //Act
            var binWidth = BinWidthCalculator.Calculate(orderItems);

            //Assert
            Assert.That(binWidth, Is.EqualTo(expectedBinWidth));
        }

        [Test]
        public void CalculateBinWidth_OfMultipleProducts_ReturnsExpectedBinWidth()
        {
            //Arrange
            var orderItems = new List<OrderItem>
            {
                    CreateOrderItem(s_PhotoBookProductType, 4), //19*4
                    CreateOrderItem(s_MugProductType, 6), //94*2
                    CreateOrderItem(s_CardsProductType, 2), //4.7*2
                    CreateOrderItem(s_CanvasProductType, 2), //16*2
                    CreateOrderItem(s_CalendarProductType, 10), //10*10
            };
            var expectedBinWidth = 405.4d;

            //Act
            var binWidth = BinWidthCalculator.Calculate(orderItems);

            //Assert
            Assert.That(binWidth, Is.EqualTo(expectedBinWidth));
        }

        private OrderItem CreateOrderItem(string productType, int quantity)
        {
            return new OrderItem { ProductType = productType, Quantity = quantity };
        }
    }
}
