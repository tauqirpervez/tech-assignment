﻿using NUnit.Framework;
using PhotoProducts.Api.Core;
using PhotoProducts.Api.Core.Products;
using System;

namespace PhotoProducts.Api.Test.Core
{
    [TestFixture]
    internal class ProductFactoryTest
    {
        [TestCase("photoBook", typeof(PhotoBook))]
        [TestCase("calendar", typeof(Calendar))]
        [TestCase("canvas", typeof(Canvas))]
        [TestCase("cards", typeof(Cards))]
        [TestCase("mug", typeof(Mug))]
        public void Create_ValidProductType_CreatesExpectedProduct(string productType, Type expectedType)
        {
            //Act
            var product = ProductFactory.Create(productType);

            //Assert
            Assert.That(product.GetType(), Is.EqualTo(expectedType));
        }

        [Test]
        public void Create_InvalidProductType_ThrowsArgumentException()
        {
            //Arrange
            var productType = "InvalidProductType";

            //Act/Assert
            Assert.That(() => { ProductFactory.Create(productType); }, Throws.TypeOf<ArgumentException>());
        }
    }
}
