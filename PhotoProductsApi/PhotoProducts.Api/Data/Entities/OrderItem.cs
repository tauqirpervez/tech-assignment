﻿using PhotoProducts.Api.Core;
using System.ComponentModel.DataAnnotations;

namespace PhotoProducts.Api.Data.Entities
{
    public class OrderItem
    {
        [Key]
        public int OrderItemId { get; set; }

        public ProductType ProductType { get; set; }
        public int Quantity { get; set; }

        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
}
