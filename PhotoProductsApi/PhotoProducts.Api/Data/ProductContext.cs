﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PhotoProducts.Api.Data.Entities;

namespace PhotoProducts.Api.Data
{
    public class ProductContext : DbContext
    {
        private const string _connectionStringName = "PhotoProductsDb";
        private readonly IConfiguration _configuration;

        public DbSet<Order> Orders { get; set; }

        public ProductContext(DbContextOptions options, IConfiguration configuration) : base(options)
        {
            this._configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString(_connectionStringName));
        }
    }
}
