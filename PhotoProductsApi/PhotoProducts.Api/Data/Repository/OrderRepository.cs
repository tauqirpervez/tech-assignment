﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoProducts.Api.Model;
using System.Collections.Generic;
using System.Linq;

namespace PhotoProducts.Api.Data.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ProductContext _productContext;
        private readonly IMapper _mapper;

        public OrderRepository(ProductContext productContext, IMapper mapper)
        {
            this._productContext = productContext;
            this._mapper = mapper;
        }

        void IOrderRepository.Create(Order order)
        {
            var orderEntity = _mapper.Map<Entities.Order>(order);
            _productContext.Add(orderEntity);
            _productContext.SaveChanges();
        }

        IList<Order> IOrderRepository.GetAll()
        {
            var orders = _productContext.Orders.Include(o => o.Items);
            return _mapper.Map<List<Model.Order>>(orders);
        }

        Order IOrderRepository.GetById(int id)
        {
            var order = _productContext.Orders.Include(o => o.Items).FirstOrDefault(o => o.Id == id);
            return _mapper.Map<Model.Order>(order);
        }
    }
}