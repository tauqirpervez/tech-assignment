﻿using PhotoProducts.Api.Model;
using System.Collections.Generic;

namespace PhotoProducts.Api.Data.Repository
{
    public interface IOrderRepository
    {        
        void Create(Order order);

        IList<Order> GetAll();

        Order GetById(int id);
    }
}