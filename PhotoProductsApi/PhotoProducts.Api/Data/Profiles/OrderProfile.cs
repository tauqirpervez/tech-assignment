﻿using AutoMapper;

namespace PhotoProducts.Api.Data.Profiles
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            this.CreateMap<Model.Order, Entities.Order>()
                .ForMember(orderEntity => orderEntity.Id, mce => mce.MapFrom(orderModel => orderModel.OrderId))
                .ReverseMap();
            this.CreateMap<Model.OrderItem, Entities.OrderItem>()
                .ForMember(itemEntity => itemEntity.ProductType, mce => mce.MapFrom(itemModel => itemModel.ProductType))
                .ReverseMap();
        }
    }
}
