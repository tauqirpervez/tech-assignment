﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using PhotoProducts.Api.Data.Repository;
using PhotoProducts.Api.Model;
using System.Collections.Generic;
using System.Linq;

namespace PhotoProducts.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderRepository _orderRepository;
        private readonly LinkGenerator _linkGenerator;

        public OrderController(IOrderRepository orderRepository, LinkGenerator linkGenerator)
        {
            _orderRepository = orderRepository;
            this._linkGenerator = linkGenerator;
        }

        [HttpGet]
        public ActionResult<List<Order>> Get()
        {
            return Ok(_orderRepository.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<Order> Get(int id)
        {
            var order = _orderRepository.GetById(id);
            if(order == null)
            {
                return NotFound(id);
            }
            return Ok(order);
        }

        [HttpPost]
        public ActionResult<double> Post(Order order)
        {
            var existingOrder = _orderRepository.GetById(order.OrderId);
            if(existingOrder != null)
            {
                return BadRequest($"Order id {order.OrderId} already exists.");
            }

            _orderRepository.Create(order);
            var orderUri =_linkGenerator.GetPathByAction("Get", "Order", new { id = order.OrderId });
            return Created(orderUri, order.BinWidth);
        }
    }
}