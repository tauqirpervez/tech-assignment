﻿namespace PhotoProducts.Api.Core.Products
{
    public class Canvas : IProduct
    {
        private const double _photoBookWidth = 16;

        double IProduct.Width => _photoBookWidth;

        ProductType IProduct.ProductType => ProductType.Canvas;
    }
}
