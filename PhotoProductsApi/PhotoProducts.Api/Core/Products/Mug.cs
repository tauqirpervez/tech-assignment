﻿namespace PhotoProducts.Api.Core.Products
{
    public class Mug : IProduct, IStackable
    {
        private const double _photoBookWidth = 94;

        double IProduct.Width => _photoBookWidth;

        ProductType IProduct.ProductType => ProductType.Mug;

        int IStackable.MaxStackCount => 4;
    }
}
