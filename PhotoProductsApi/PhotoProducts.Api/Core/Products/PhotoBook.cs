﻿namespace PhotoProducts.Api.Core.Products
{
    public class PhotoBook : IProduct
    {
        private const double _photoBookWidth = 19;

        double IProduct.Width => _photoBookWidth;

        ProductType IProduct.ProductType  => ProductType.PhotoBook;
    }
}
