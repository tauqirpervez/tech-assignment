﻿namespace PhotoProducts.Api.Core.Products
{
    public class Calendar : IProduct
    {
        private const double _photoBookWidth = 10;

        double IProduct.Width => _photoBookWidth;

        ProductType IProduct.ProductType => ProductType.Calendar;
    }
}
