﻿namespace PhotoProducts.Api.Core.Products
{
    public class Cards : IProduct
    {
        private const double _photoBookWidth = 4.7;

        double IProduct.Width => _photoBookWidth;

        ProductType IProduct.ProductType => ProductType.Cards;
    }
}
