﻿namespace PhotoProducts.Api.Core
{
    public interface IStackable
    {
        public int MaxStackCount { get; }
    }
}
