﻿namespace PhotoProducts.Api.Core
{
    public enum ProductType
    {
        PhotoBook,
        Calendar,
        Canvas,
        Cards,
        Mug
    }
}
