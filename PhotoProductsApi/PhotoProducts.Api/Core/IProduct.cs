﻿namespace PhotoProducts.Api.Core
{
    public interface IProduct
    {
        public ProductType ProductType { get; }
        public double Width { get; }
    }
}
