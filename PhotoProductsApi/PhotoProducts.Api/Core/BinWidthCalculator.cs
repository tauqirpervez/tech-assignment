﻿using PhotoProducts.Api.Model;
using System.Collections.Generic;

namespace PhotoProducts.Api.Core
{
    public static class BinWidthCalculator
    {
        public static double Calculate(IList<OrderItem> orderItems)
        {
            double minBinWidth = 0;
            foreach (var orderItem in orderItems)
            {
                var item = ProductFactory.Create(orderItem.ProductType);
                var binWidth = item.Width * orderItem.Quantity;
                if (item is IStackable stackableItem)
                {
                    var stacks = orderItem.Quantity / stackableItem.MaxStackCount;
                    if (orderItem.Quantity % stackableItem.MaxStackCount != 0)
                    {
                        stacks++;
                    }
                    binWidth = item.Width * stacks;
                }

                minBinWidth += binWidth;
            }

            return minBinWidth;
        }
    }
}
