﻿using PhotoProducts.Api.Core.Products;
using System;

namespace PhotoProducts.Api.Core
{
    public class ProductFactory
    {
        public static IProduct Create(string productType)
        {
            switch (productType.ToLower())
            {
                case "photobook":
                    return new PhotoBook();
                case "calendar":
                    return new Calendar();
                case "canvas":
                    return new Canvas();
                case "cards":
                    return new Cards();
                case "mug":
                    return new Mug();
                default:
                    throw new ArgumentException($"Invalid Product Type: {productType}.");
            }
        }
    }
}
