﻿using PhotoProducts.Api.Core;
using System.Collections.Generic;

namespace PhotoProducts.Api.Model
{
    public class Order
    {
        public int OrderId { get; set; }

        public IList<OrderItem> Items { get; set; }

        public double BinWidth => BinWidthCalculator.Calculate(Items);
    }
}