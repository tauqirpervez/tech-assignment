﻿namespace PhotoProducts.Api.Model
{
    public class OrderItem
    {
        public string ProductType { get; set; }
        public int Quantity { get; set; }
    }
}
